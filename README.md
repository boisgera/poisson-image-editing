Poisson Image Editing
================================================================================

The pdf document is available here:
<https://bitbucket.org/complexanalysispie/poisson-image-editing/downloads>

Instead, to generate the pdf yourself, 
on a computer running Linux, 
with an unrestricted access to the Internet:

 1. Install [git](https://git-scm.com/), then execute:

        $ git clone git@bitbucket.org:complexanalysispie/poisson-image-editing.git

 2. Install [docker](https://www.docker.com), then get the permission to run it.

    Instructions:

      1. <https://docs.docker.com/engine/installation/ubuntulinux/>
        
      2. <https://docs.docker.com/engine/installation/ubuntulinux/#create-a-docker-group>

 3. Enter the project repository, then build the pdf file:

        $ cd poisson-image-editing/
        $ ./docker/build

    This third step may take a while because you download 
    [a large docker image](https://hub.docker.com/r/boisgera/pdf/).
    Subsequent builds will be much faster.
